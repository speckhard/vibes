# References

## How to cite `FHI-vibes`?

[Knoop et al., (2020). FHI-vibes: Ab Initio Vibrational Simulations. Journal of Open Source Software, 5(56), 2671](https://doi.org/10.21105/joss.02671)

```
@article{Knoop2020,
  doi = {10.21105/joss.02671},
  url = {https://doi.org/10.21105/joss.02671},
  year = {2020},
  publisher = {The Open Journal},
  volume = {5},
  number = {56},
  pages = {2671},
  author = {Florian Knoop and Thomas A. R. Purcell and Matthias Scheffler and Christian Carbogno},
  title = {FHI-vibes: _Ab Initio_ Vibrational Simulations},
  journal = {Journal of Open Source Software}
}
```

## Work that was performed using `FHI-vibes`

### "Anharmonicity measure for materials"
[F. Knoop, T.A.R. Purcell, M. Scheffler, and C. Carbogno, Phys. Rev. Materials **4**, 083809 (2020)](https://doi.org/10.1103/PhysRevMaterials.4.083809), [arXiv:2006.14672](https://arxiv.org/abs/2006.14672)

```
@article{PhysRevMaterials.4.083809,
title = {Anharmonicity measure for materials},
author = {Knoop, Florian and Purcell, Thomas A. R. and Scheffler, Matthias and Carbogno, Christian},
journal = {Phys. Rev. Materials},
volume = {4},
issue = {8},
pages = {083809},
numpages = {12},
year = {2020},
month = {Aug},
publisher = {American Physical Society},
doi = {10.1103/PhysRevMaterials.4.083809},
url = {https://link.aps.org/doi/10.1103/PhysRevMaterials.4.083809}
}
```
